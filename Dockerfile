FROM ubuntu:22.04
LABEL maintainer="iserbin@icloud.com"

USER root

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y python 
RUN apt-get install -y git python build-essential dosfstools make bison bc libssl-dev libncurses-dev \ 
    git-core gnupg flex gperf zip zlib1g-dev gcc-multilib g++-multilib libc6-dev-i386 lib32ncurses5-dev \ 
    x11proto-core-dev libx11-dev lib32z-dev ccache libgl1-mesa-dev libxml2-utils xsltproc unzip imagemagick glances mc \ 
    lib32z1 curl libswitch-perl rsync


# WORKDIR /home/
# COPY . .
# WORKDIR /home/u-boot

# RUN export CROSS_COMPILE=aarch64-linux-gnu-
# RUN expot ARCH=arm64
# RUN CROSS_COMPILE=aarch64-linux-gnu- make rpi_4_defconfig
# RUN CROSS_COMPILE=aarch64-linux-gnu- make -j4